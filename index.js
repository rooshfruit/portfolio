const express = require('express');
const calculate = require('calculator');
const app = express();

app.use('/', (request, response) => {
    calculate("MSFT", 1000, 0.2, data => {
        response.contentType = "application/json";
        response.send(data);
    });
});

app.listen(3000, () => {
    console.log("Listening on port 3000");
});